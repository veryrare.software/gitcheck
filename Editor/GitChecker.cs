using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[InitializeOnLoad]
public class GitChecker
{
    static bool WantsToQuit()
    {
        int quitWithoutCommit = EditorUtility.DisplayDialogComplex("QUIT WITHOUT COMMIT?","Do you really want to quit without commit?", "YEA, quit", "NAH, cancel", "COMMIT and push");
        if ( quitWithoutCommit == 2)
        {
            var w = EditorWindow.GetWindow<EditorWindow>("Git for Unity");
            // try to find the Git Window...
            if ( w == null || w.title != "Git for Unity") 
            {
                // if not found, try to execute the git menu item command to create the window
                if (!EditorApplication.ExecuteMenuItem("Window/Git") )
                {
                    Debug.Log("Git for Unity is null and could not execute menu item Window/Git.");

                    if (EditorUtility.DisplayDialog(
                        "'Git for Unity' not Found", 
                        "Install Git for Unity package https://github.com/spoiledcat/git-for-unity#packages/com.spoiledcat.git/latest via Package Manager and open the 'Window/Git' window", 
                        "Open 'Git for Unity' package URL"
                    ))
                    {
                        Application.OpenURL("https://github.com/spoiledcat/git-for-unity#packages/com.spoiledcat.git/latest");
                    }
                } else {
                    Debug.Log("Executed menu item Window/Git");
                }
            } else 
            {
                Debug.Log("Focusing 'Git for Unity' Window.. type:" + w.GetType());
                w.Show();
                w.Focus();
            }
        }

        return quitWithoutCommit == 0;
    }

    static GitChecker()
    {
        Debug.Log("GitChecker Initialized");
        EditorApplication.wantsToQuit += WantsToQuit;
    }
}
#endif